import 'dotenv/config';
import "reflect-metadata"
import config from "./config/config";
import app from "./server/server";
import {logf} from "./log";

app.listen(
    config.port,
    config.bind,
    () => logf(__filename, `Launched ${config.bind}:${config.port}`)
);