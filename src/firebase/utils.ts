import {Notification} from "../types/notification";
import {getMessaging} from "firebase-admin/messaging";
import firebase from "./firebase";
import data from "../data/data";
import {Device} from "../data/models/Device";
import {logf} from "../log";

function mapToString(data: Record<string, any>): Record<string, string> {
    return Object.fromEntries(Object.entries(data).filter(([key, value]) => key !== undefined && value !== undefined && key !== null && value !== null).map(([key, value]) => [key, String(value)]));
}

function flattenNotification(notification: Notification): Record<string, string> {
    const {properties, ...rest} = notification;

    Object.keys(rest).forEach((k) => {
        if (properties) {
            delete properties[k];
        }
    });

    return {
        ...mapToString(rest),
        ...mapToString(properties ?? {}),
    };
}

/**
 * Sends a notification in the form of a data message and notification to all devices currently listed in the database
 * @param notification the notification data to transmit
 * @param targets the set of targets if this should be more narrow - otherwise it will target all devices in the
 * database
 */
export async function sendNotification(notification: Notification, targets?: string[]) {
    let devices = targets ?? (await (await data).manager.find(Device)).map((e) => e.identifier);

    logf(__filename, 'sending multicast notification + data to ', devices.length, 'devices');
    const {correlation, ...rest} = flattenNotification(notification);

    return Promise.all([
        getMessaging(firebase).sendMulticast({
            tokens: devices,
            data: mapToString({
                ...rest,
                correlation_id: correlation,
            }),
        }),
        getMessaging(firebase).sendMulticast({
            tokens: devices,
            notification: {
                title: `${notification.status} // ${notification.module}: ${notification.summary}`,
                body: notification.detail,
            },
        }),
    ]).then(async (response) => {
        if (response[0].failureCount + response[1].failureCount > 0) {
            const tokens = [...response[0].responses, ...response[1].responses].map((e, idx) => e.success ? undefined : devices[idx]).filter((e) => e) as string[];
            logf(__filename, 'tokens', tokens, 'failed');
            return (await data).manager.getRepository(Device).delete(tokens).then(() => Promise.resolve());
        }

        logf(__filename, 'all messages sent');
        return Promise.resolve();
    })
}