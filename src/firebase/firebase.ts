import {initializeApp} from 'firebase-admin/app';
import {credential} from "firebase-admin";
import applicationDefault = credential.applicationDefault;

export default initializeApp({
    credential: applicationDefault(),
})