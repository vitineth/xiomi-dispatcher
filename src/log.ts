const appPath = require('app-root-path').toString();

export default function log(tag: string, message: string, ...data: any[]) {
    logf(__filename, `\u001b[36m[${tag}]: ${message}\u001b[0m`, ...data);
}

export function logf(filename: string, message: string, ...data: any[]) {
    log(filename.replace(appPath, "").substring(1), message, ...data);
}