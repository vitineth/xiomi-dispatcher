import fs from 'fs';
import * as zod from 'zod';
import * as path from "path";
import * as process from "process";
import log, {logf} from "../log";

/**
 * The default validator the configuration. Contains binding information for the port and IP address to which the server
 * should bind
 */
const validator = zod.object({
    bind: zod.string(),
    port: zod.number(),
});

// Load content from disk as a string
logf(__filename, 'Loading config');
let content: string;
try {
    content = fs.readFileSync(path.join(__dirname, '..', '..', 'config', 'config.json'), {encoding: 'utf-8'})
} catch (e) {
    logf(__filename, "Failed to load config file from", path.join(__dirname, '..', '..', 'config.json'));
    process.exit(1);
}

// Parse the content as JSON
logf(__filename, 'Parsing JSON');
let json: any;
try {
    json = JSON.parse(content);
} catch (e: any) {
    logf(__filename, "Failed to load JSON from the config file", e.message);
    process.exit(1);
}

// Validate the configuration
logf(__filename, 'Validating configuration');
let validate = validator.safeParse(json);
if (!validate.success) {
    logf(__filename, "Failed to parse config - " + validate.error.format()._errors.join("; "));
    process.exit(1);
}

// And export once loaded
logf(__filename, 'Configuration loaded successfully!');
export default validate.data;
