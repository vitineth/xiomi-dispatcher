import NotificationValidator from "./notification";
import * as zod from 'zod';

const NotificationRequestValidator = NotificationValidator.extend({
    authentication: zod.object({
        nonce: zod.number(),
        identifier: zod.string().uuid(),
        hash: zod.string(),
    })
});

export type NotificationRequest = zod.infer<typeof NotificationRequestValidator>;
export default NotificationRequestValidator;