import * as zod from "zod";

const RegisterRequestValidator = zod.object({
    identifier: zod.string(),
});

export type RequestRegister = zod.infer<typeof RegisterRequestValidator>;
export default RegisterRequestValidator;