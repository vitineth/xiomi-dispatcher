import * as zod from 'zod';

const NotificationValidator = zod.object({
    uid: zod.string().uuid().optional(),
    correlation: zod.string().uuid().optional(),
    status: zod.enum([
        'NOTIFY',
        'ALERT',
        'CRITICAL',
        'RESOLVING',
        'RESOLVED',
        'UNKNOWN',
        'MESSAGE',
    ]),
    module: zod.string(),
    topic: zod.string(),
    summary: zod.string(),
    detail: zod.string().optional(),
    properties: zod.record(zod.string(), zod.string()).optional(),
    timestamp: zod.number().optional(),
});

export type Notification = zod.infer<typeof NotificationValidator>;
export default NotificationValidator;