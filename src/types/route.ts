import {NextFunction, RequestHandler} from "express";

export default function route(method: 'get' | 'post', path: string | RegExp, handler: RequestHandler) {
    return {method, path, handler}
}