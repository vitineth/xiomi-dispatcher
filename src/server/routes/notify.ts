import route from "../../types/route";
import {constants} from "http2";
import NotificationRequestValidator, {NotificationRequest} from "../../types/notification-request";
import data from "../../data/data";
import {Client} from "../../data/models/Client";
import crypto from 'crypto';
import {Nonce} from "../../data/models/Nonce";
import {sendNotification} from "../../firebase/utils";
import {v4} from 'uuid';
import {logf} from "../../log";

export function produceHashable(test: NotificationRequest): string {
    return `${test.authentication.nonce}.${test.uid}.${test.correlation ?? ''}.${test.status}.${test.module}.${test.topic}.${test.summary}.${test.detail ?? ''}.${test.timestamp ?? 0}.${Object.entries(test.properties ?? {}).map(([key, value]) => `${key}.${value}`)}`
}

function verifyHash(client: Client, request: NotificationRequest) {
    const verifier = crypto.createVerify('RSA-SHA256');
    verifier.write(produceHashable(request));
    verifier.end();

    return verifier.verify(client.publicKey, request.authentication.hash, 'base64');
}

export default route(
    'post',
    '/notify',
    async (req, res) => {
        if (!req.body) {
            logf(__filename, 'Body is not specified');
            res.status(constants.HTTP_STATUS_BAD_REQUEST).end();
            return;
        }

        // Make sure request is valid
        const parse = NotificationRequestValidator.safeParse(req.body);
        if (!parse.success) {
            logf(__filename, 'Request failed to validate', parse.error);
            res.status(constants.HTTP_STATUS_BAD_REQUEST).end();
            return;
        }
        const request = parse.data;

        // Check client exists
        const client = await (await data).manager.findOne(Client, {
            where: {
                uid: request.authentication.identifier,
            },
        });
        if (!client) {
            logf(__filename, 'Client identifier does not exist');
            res.status(constants.HTTP_STATUS_UNAUTHORIZED).end();
            return;
        }

        // Check client is not deactivated
        if (client.deactivated) {
            logf(__filename, 'Client has been deactivated');
            res.status(constants.HTTP_STATUS_UNAUTHORIZED).end();
            return;
        }

        // Check nonce hasn't been used
        const nonceLookup = await (await data).manager.find(Nonce, {
            where: {
                nonce: request.authentication.nonce,
            },
        });
        if (nonceLookup.length > 0) {
            logf(__filename, 'Nonce has already been used');
            res.status(constants.HTTP_STATUS_UNAUTHORIZED).end();
            return;
        }
        await (await data).manager.insert(Nonce, {
            nonce: request.authentication.nonce,
            used: Math.floor(Date.now() / 1000),
        });

        // Check the authentication is accurate
        if (!verifyHash(client, request)) {
            logf(__filename, 'Hash validation failed');
            res.status(constants.HTTP_STATUS_UNAUTHORIZED).end();
            return;
        }

        if (!request.timestamp) request.timestamp = Math.floor(Date.now() / 1000);
        if (!request.uid) request.uid = v4();
        if (!request.detail) request.detail = '';

        // At this point the notification is fine so ship it
        const {authentication, ...rest} = request;

        try {
            await sendNotification(rest);
            logf(__filename, `Client ${client.uid} sent notification`, rest);
            res.status(constants.HTTP_STATUS_OK).end();
        } catch (e) {
            res.status(constants.HTTP_STATUS_INTERNAL_SERVER_ERROR).end();
            logf(__filename, 'Failed to send notification', e);
        }
    }
)