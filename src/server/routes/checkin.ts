import route from "../../types/route";
import {constants} from "http2";
import RegisterRequestValidator from "../../types/register-request";
import data from "../../data/data";
import {Device} from "../../data/models/Device";
import {logf} from "../../log";

export default route(
    'post',
    '/checkin',
    async (req, res) => {
        if (!req.body) {
            logf(__filename, 'Body is not specified');
            res.status(constants.HTTP_STATUS_BAD_REQUEST).end();
            return;
        }

        // Make sure request is valid
        const parse = RegisterRequestValidator.safeParse(req.body);
        if (!parse.success) {
            logf(__filename, 'Request failed to validate');
            res.status(constants.HTTP_STATUS_BAD_REQUEST).end();
            return;
        }
        const request = parse.data;

        // Check client exists
        const repository = (await data).manager.getRepository(Device);
        const client = await repository.findOne({
            where: {
                identifier: request.identifier,
            },
        });
        if (!client) {
            try {
                await repository.insert({
                    identifier: request.identifier,
                    registered: Math.floor(Date.now() / 1000),
                    checkin: Math.floor(Date.now() / 1000),
                });
                res.status(constants.HTTP_STATUS_OK).end();
            } catch (e) {
                logf(__filename, 'Failed to register', e);
                res.status(constants.HTTP_STATUS_INTERNAL_SERVER_ERROR).end();
            }
        } else {
            try {
                client.checkin = Math.floor(Date.now() / 1000);
                await repository.save(client);
                logf(__filename, `Client ${client.identifier} checked in - ${client.checkin}`);
                res.status(constants.HTTP_STATUS_OK).end();
            } catch (e) {
                logf(__filename, 'Failed to update check in', e);
                res.status(constants.HTTP_STATUS_INTERNAL_SERVER_ERROR).end();
            }
        }
    }
)