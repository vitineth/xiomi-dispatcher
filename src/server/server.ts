import express, {Response} from "express";
import notify from './routes/notify';
import register from "./routes/checkin";
import bodyParser from "body-parser";
import {constants} from "http2";
import {logf} from "../log";

const app = express();
app.use(bodyParser.json({strict: true}))
const routes = [notify, register];

function handleError(e: any, res: Response) {
    if (!res.headersSent) {
        res.status(constants.HTTP_STATUS_INTERNAL_SERVER_ERROR).end();
    }

    logf(__filename, 'Failed to handle request', e);
}

routes.forEach(({method, path, handler}) => {
    app[method](path, (req, res, next) => {
        try {
            Promise.resolve(handler(req, res, next)).catch((e) => handleError(e, res));
        } catch (e) {
            handleError(e, res);
        }
    });
});

export default app;