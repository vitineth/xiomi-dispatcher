import minimist from 'minimist';
import fs from "fs";
import crypto from "crypto";
import {Client} from "../data/models/Client";
import * as uuid from "uuid";
import data from "../data/data";
import child from 'child_process';
import {Device} from "../data/models/Device";
import {produceHashable} from "../server/routes/notify";
import {NotificationRequest} from "../types/notification-request";
import prompts from "prompts";
import fetch from "node-fetch";
import {logf} from "../log";

const text = (prompt: string, validate?: (v: string) => boolean, required: boolean = false): Promise<string> => new Promise(async (resolve, reject) => {
    const result = await prompts({
        type: 'text',
        message: prompt,
        validate,
        name: '__',
        onState: (state) => {
            if (state.aborted) {
                process.nextTick(() => {
                    process.exit(0);
                })
            }
        }
    }, {
        onCancel: () => required,
    });

    if (typeof (result.__) !== 'string') return text(prompt, validate, required).then(resolve).catch(reject);
    else resolve(result.__);
});
const yesNo = (prompt: string, required: boolean = false): Promise<boolean> => new Promise(async (resolve, reject) => {
    const result = await prompts({
        type: 'confirm',
        message: prompt,
        name: '__',
        onState: (state) => {
            if (state.aborted) {
                process.nextTick(() => {
                    process.exit(0);
                })
            }
        }
    }, {
        onCancel: () => required,
    });

    if (typeof (result.__) !== 'boolean') return yesNo(prompt, required).then(resolve).catch(reject);
    else resolve(result.__);
});
const select = <T extends string, >(prompt: string, options: T[], required: boolean = false): Promise<T> => new Promise(async (resolve, reject) => {
    const result = await prompts({
        type: 'select',
        choices: options.map((e) => ({title: e, value: e})),
        message: prompt,
        name: '__',
        onState: (state) => {
            if (state.aborted) {
                process.nextTick(() => {
                    process.exit(0);
                })
            }
        }
    }, {
        onCancel: () => required,
    });

    if (typeof (result.__) !== 'string') return select(prompt, options, required).then(resolve).catch(reject);
    else resolve(result.__ as T);
});

function generateKeys() {
    const privateKey = child.execSync("openssl genpkey -algorithm RSA -pkeyopt rsa_keygen_bits:4096 -pkeyopt rsa_keygen_pubexp:3", {
        stdio: ['ignore', 'pipe', 'ignore'],
        encoding: 'utf8'
    });
    if (!privateKey.startsWith("-----BEGIN PRIVATE KEY-----")) {
        logf(__filename, 'Failed to generate key as the content did not start correctly - ', privateKey);
        throw new Error("Failed to generate private key");
    }

    return new Promise<{ publicKey: string, privateKey: string }>((resolve, reject) => {
        const publicKeyProcess = child.spawn("/usr/bin/openssl", ["pkey", "-in", "-", "-pubout"], {stdio: 'pipe'});
        let data = "";
        let stderr = "";
        publicKeyProcess.stdin?.write(privateKey, () => publicKeyProcess.stdin.end());
        publicKeyProcess.stderr?.on("data", (d) => stderr += d.toString('utf8'));
        publicKeyProcess.stdout?.on("data", (d) => data += d.toString('utf8'));

        publicKeyProcess.on('error', reject)
        publicKeyProcess.on('exit', (code: number) => {
            if (code === 0) resolve({
                publicKey: data,
                privateKey,
            });
            else reject(new Error(stderr));
        });
    });
}

const splitWords = (length: number, text: string): string[] => {
    let lines = [];
    let currentLine = [];
    const words = text.split(" ");

    let index = 0;
    while (index < words.length) {
        const word = words[index];
        if (word.length === 0) {
            index++;
            continue;
        }

        if (currentLine.join(' ').length + word.length >= length) {
            if (currentLine.length === 0) {
                currentLine.push(word.substring(0, length));
                words[index] = word.substring(length);
            } else {
                lines.push(currentLine.join(' '));
                currentLine = [];
            }
        } else {
            currentLine.push(word);
            index++;
        }
    }

    if (currentLine.length > 0) lines.push(currentLine.join(" "));
    return lines;
}

const verifyKeys = (publicKey: string, privateKey: string) => {
    try {
        const signer = crypto.createSign('RSA-SHA256');
        signer.write("a");
        signer.end();

        const signature = signer.sign(privateKey, 'base64');

        const verifier = crypto.createVerify('RSA-SHA256');
        verifier.write("a");
        verifier.end();

        return verifier.verify(publicKey, signature, 'base64');
    } catch (e) {
        return false;
    }
}

const HELP_TEXT = `xiomi-dispatcher <=> xiomi-notifier

cli\u001b[1m client list\u001b[0m       - list all the currently registered clients
cli\u001b[1m client add \u001b[0m       - add a new client
           add --uid {uuid} --public {keyfile: filepath} --private {keyfile: filepath} --purpose {purpose: string} [--deactivated]
           add --generate --purpose {purpose: string} [--deactivated]
cli\u001b[1m client deactivate\u001b[0m - deactivate a given client
           deactivate [uuid]
cli\u001b[1m client delete\u001b[0m     - delete a given client
           delete [uuid]

cli\u001b[1m device list\u001b[0m       - list all registered devices
cli\u001b[1m device add\u001b[0m        - add a new device FCM token
           add {FCM: string}
cli\u001b[1m device delete\u001b[0m     - delete a FCM token from distribution
           delete {uid: number}
           
cli\u001b[1m notify\u001b[0m            - send a notification to all clients
`

const argv = minimist(process.argv.slice(2))
if (argv._.length < 1) {
    console.error(HELP_TEXT);
    process.exit(1);
}

if (argv._[0] === "notify") {
    (async () => {
        if (argv.client) console.log("* Using client ID " + argv.client);
        if (argv.key) console.log("* Using private key file " + argv.key);
        const client = argv.client ?? await text('Client UUID', uuid.validate);
        const privateKey = argv.key ?? await text('Private key file', (v) => fs.existsSync(v));
        const correlate = await yesNo('Do you want to correlate this request?');
        let correlationID: string | undefined | false = undefined;
        if (correlate) {
            correlationID = await text('Correlation UUID', uuid.validate);
        }
        const status = await select('Message format', [
            'NOTIFY',
            'ALERT',
            'CRITICAL',
            'RESOLVING',
            'RESOLVED',
            'UNKNOWN',
            'MESSAGE',
        ]);
        const module = await text('Module');
        const topic = await text('Topic');
        const summary = await text('Summary');
        const detail = await text('Detail');
        let properties: Record<string, string> | undefined = undefined;
        const addProperties = await yesNo('Add additional properties?');
        if (addProperties) {
            do {
                const propertyName = await text('Property name');
                const propertyValue = await text('Property value');
                if (propertyName && propertyValue) {
                    if (!properties) properties = {};
                    properties[propertyName] = propertyValue;
                }
            } while (await yesNo("Add another property?"));
        }

        if (client && privateKey && (!correlate || (correlate && correlationID)) && status && module && topic && summary) {
            const nonce = Math.floor(Math.random() * (Number.MAX_SAFE_INTEGER - 1));
            const notificationRequest: NotificationRequest = {
                uid: uuid.v4().toString(),
                detail: detail.trim().length === 0 ? undefined : detail.trim(),
                timestamp: Math.floor(Date.now() / 1000),
                status,
                summary,
                module,
                topic,
                properties,
                correlation: correlate ? correlationID as string : undefined,
                authentication: {
                    nonce,
                    hash: 'pending',
                    identifier: client,
                }
            }

            const hashable = produceHashable(notificationRequest);
            const signer = crypto.createSign('RSA-SHA256');
            signer.write(hashable);
            signer.end();
            notificationRequest.authentication.hash = signer.sign(fs.readFileSync(privateKey, {encoding: 'utf8'}), 'base64');

            const server = argv.server ?? (argv.secure ? 'https://' : 'http://') + 'localhost:9090';
            fetch(server + (server.endsWith("/") ? 'notify' : '/notify'), {
                method: 'POST',
                body: JSON.stringify(notificationRequest),
                headers: {
                    'Content-Type': 'application/json',
                },
            }).then((result) => {
                if (result.status === 200) {
                    console.log('Sent successfully!');
                } else if (result.status === 401) {
                    console.log('Request was unauthorized!');
                } else {
                    console.log('Request response: ' + result.status);
                }
            }).catch((e: any) => {
                console.error('Failed to notify', e);
            })
        }
    })();
} else if (argv._.length >= 2) {
    if (argv._[0] === 'client') {
        if (argv._[1] === 'add') {
            if (['uid', 'public', 'private', 'purpose'].every((e) => Object.prototype.hasOwnProperty.call(argv, e))) {
                const privateKey = fs.readFileSync(argv.private, {encoding: 'utf-8'});
                const publicKey = fs.readFileSync(argv.public, {encoding: 'utf-8'});

                if (!verifyKeys(publicKey, privateKey)) {
                    console.error('Invalid keys - must be suitable for RSA-SHA256 signing and verifying and must be a matching set');
                    process.exit(1);
                }

                try {
                    uuid.parse(argv.uid);
                } catch (e) {
                    console.error('Invalid UID - must be a valid UUID');
                    process.exit(1);
                }

                const client = new Client();
                client.uid = argv.uid;
                client.publicKey = publicKey;
                client.created = Math.floor(Date.now() / 1000);
                client.deactivated = argv.deactivated ? Math.floor(Date.now() / 1000) : undefined;
                client.purpose = argv.purpose;
                data.then((d) => d.manager.insert(Client, client))
                    .then(() => {
                        console.log('New client is registered!');
                    })
                    .catch((e: any) => {
                        console.log('Failed to insert new client: ' + e.message);
                        process.exit(1);
                    });
            } else if (['generate', 'purpose'].every((e) => Object.prototype.hasOwnProperty.call(argv, e))) {
                console.log('Generating keys');
                generateKeys().then(async ({publicKey, privateKey}) => {
                    const client = new Client();
                    client.uid = uuid.v4();
                    client.deactivated = argv.deactivated ? Math.floor(Date.now() / 1000) : undefined;
                    client.purpose = argv.purpose;
                    client.publicKey = publicKey;
                    client.created = Math.floor(Date.now() / 1000);

                    try {
                        fs.writeFileSync(`${client.uid}.private.pem`, privateKey, {encoding: 'utf8'});

                        await (await data).manager.insert(Client, client);
                        console.log('New client has been generated:');
                        console.log(`\u001b[1mUID\u001b[0m         : ${client.uid}`);
                        console.log(`\u001b[1mDeactivated\u001b[0m : ${client.deactivated}`);
                        console.log(`\u001b[1mPurpose\u001b[0m     : ${client.purpose}`);
                        console.log(`\u001b[1mPublic Key\u001b[0m  : ${client.publicKey}`);
                        console.log(`\u001b[1mCreated\u001b[0m     : ${client.created}`);
                        console.log(`\u001b[1mPrivate Key\u001b[0m : key has been written to ${client.uid}.private.pem`);
                    } catch (e: any) {
                        console.error('Failed to save the new client: ', e.message);
                    }
                }).catch((e: any) => {
                    console.error('Failed to generate keys: ', e.message);
                    process.exit(1);
                })
            } else {
                console.error('Invalid parameter set');
                process.exit(1);
            }
        } else if (argv._[1] === 'deactivate' && argv._.length === 3) {
            data.then(async (d) => {
                const client = await d.manager.findOne(Client, {
                    where: {
                        uid: argv._[2]
                    }
                });

                if (!client) {
                    console.error('Invalid UID - client not found');
                    process.exit(1);
                }

                client.deactivated = Math.floor(Date.now() / 1000);
                await d.manager.save(Client, client);
                console.log('Client deactivated');
            }).catch((e: any) => {
                console.error('Failed to deactivate client: ' + e.message);
                process.exit(1);
            })
        } else if (argv._[1] === 'delete' && argv._.length === 3) {
            data.then(async (d) => {
                const client = await d.manager.delete(Client, argv._[2]);
                if (client.affected === 1) {
                    console.log('Client deactivated');
                } else {
                    throw new Error('no clients found');
                }
            }).catch((e: any) => {
                console.error('Failed to deactivate client: ' + e.message);
                process.exit(1);
            })
        } else if (argv._[1] === 'list') {
            data.then(async (d) => {
                const records = await d.manager.find(Client);
                const maxUID = Math.max(records.map((e) => e.uid.length).reduce((a, b) => Math.max(a, b), 0), "UID".length);
                const maxDeactivated = Math.max(records.map((e) => (e.deactivated ? new Date(e.deactivated * 1000).toISOString() : 'active').length).reduce((a, b) => Math.max(a, b), 0), "Deactivated".length);
                const maxCreated = Math.max(records.map((e) => new Date(e.created * 1000).toISOString().length).reduce((a, b) => Math.max(a, b), 0), "Created".length);
                const maxPurpose = Math.max(process.stdout.columns - (maxUID + maxDeactivated + maxCreated + 5), "Purpose".length);

                const pad = (s: string, l: number, direction: 'left' | 'right' = 'right') => (direction === 'right' ? (' '.repeat(Math.max(l - s.length, 0)) + s) : (s + ' '.repeat(Math.max(l - s.length, 0))));

                console.log(`|${'-'.repeat(maxUID)}|${'-'.repeat(maxDeactivated)}|${'-'.repeat(maxCreated)}|${'-'.repeat(maxPurpose)}|`);
                console.log(`|${pad('UID', maxUID)}|${pad('Deactivated', maxDeactivated)}|${pad('Created', maxCreated)}|${pad('Purpose', maxPurpose, 'left')}|`);
                console.log(`|${'-'.repeat(maxUID)}|${'-'.repeat(maxDeactivated)}|${'-'.repeat(maxCreated)}|${'-'.repeat(maxPurpose)}|`);
                records.forEach((e) => {
                    const lines = splitWords(maxPurpose, e.purpose);
                    console.log(`|${pad(e.uid, maxUID)}|${pad((e.deactivated ? new Date(e.deactivated * 1000).toISOString() : 'active'), maxDeactivated)}|${pad(new Date(e.created * 1000).toISOString(), maxCreated)}|${pad(lines[0] ?? '', maxPurpose, 'left')}|`);
                    lines.slice(1).forEach((line) => {
                        console.log(`|${pad('', maxUID)}|${pad('', maxDeactivated)}|${pad('', maxCreated)}|${pad('', maxPurpose, 'left')}|`);
                    });
                })
                console.log(`|${'-'.repeat(maxUID)}|${'-'.repeat(maxDeactivated)}|${'-'.repeat(maxCreated)}|${'-'.repeat(maxPurpose)}|`);
            })
        }
    } else if (argv._[0] === 'device') {
        if (argv._[1] === 'list') {
            data.then(async (d) => {
                const records = await d.manager.find(Device);
                const maxUID = Math.max(records.map((e) => String(e.uid).length).reduce((a, b) => Math.max(a, b), 0), "UID".length);
                const maxRegistered = Math.max(records.map((e) => new Date(e.registered * 1000).toISOString().length).reduce((a, b) => Math.max(a, b), 0), "Registered".length);
                const maxCheckin = Math.max(records.map((e) => new Date(e.checkin * 1000).toISOString().length).reduce((a, b) => Math.max(a, b), 0), "Checkin".length);
                const maxIdentifier = Math.max(process.stdout.columns - (maxUID + maxCheckin + maxRegistered + 5), "Identifier".length);

                const pad = (s: string, l: number, direction: 'left' | 'right' = 'right') => (direction === 'right' ? (' '.repeat(Math.max(l - s.length, 0)) + s) : (s + ' '.repeat(Math.max(l - s.length, 0))));

                console.log(`|${'-'.repeat(maxUID)}|${'-'.repeat(maxRegistered)}|${'-'.repeat(maxCheckin)}|${'-'.repeat(maxIdentifier)}|`);
                console.log(`|${pad('UID', maxUID)}|${pad('Registered', maxRegistered)}|${pad('Checkin', maxCheckin)}|${pad('Identifier', maxIdentifier, 'left')}|`);
                console.log(`|${'-'.repeat(maxUID)}|${'-'.repeat(maxRegistered)}|${'-'.repeat(maxCheckin)}|${'-'.repeat(maxIdentifier)}|`);
                records.forEach((e) => {
                    const lines = splitWords(maxIdentifier, e.identifier);
                    console.log(`|${pad(String(e.uid), maxUID)}|${pad(new Date(e.registered * 1000).toISOString(), maxRegistered)}|${pad(new Date(e.checkin * 1000).toISOString(), maxCheckin)}|${pad(lines[0] ?? '', maxIdentifier, 'left')}|`);
                    lines.slice(1).forEach((line) => {
                        console.log(`|${pad('', maxUID)}|${pad('', maxRegistered)}|${pad('', maxCheckin)}|${pad('', maxIdentifier, 'left')}|`);
                    });
                })
                console.log(`|${'-'.repeat(maxUID)}|${'-'.repeat(maxRegistered)}|${'-'.repeat(maxCheckin)}|${'-'.repeat(maxIdentifier)}|`);
            })
        } else if (argv._[1] === 'add') {
            if (argv._.length === 3) {
                const device = new Device();
                device.identifier = argv._[2];
                device.checkin = Math.floor(Date.now() / 1000);
                device.registered = Math.floor(Date.now() / 1000);

                data.then((d) => d.manager.insert(Device, device))
                    .then(() => console.log('Device has been registered'))
                    .catch((e: any) => {
                        console.error('Failed to insert device', e.message);
                        process.exit(1);
                    });
            } else {
                console.error('Requires FCM token');
                process.exit(1);
            }
        } else if (argv._[1] === 'delete') {
            if (argv._.length === 3) {
                let num = Number(argv._[2]);
                if (isNaN(num)) {
                    console.error('Invalid UID - find it through `device list`');
                    process.exit(1);
                }

                data.then((d) => d.manager.delete(Device, num))
                    .then(() => console.log('Device has been removed'))
                    .catch((e: any) => {
                        console.error('Failed to delete device', e.message);
                        process.exit(1);
                    });
            } else {
                console.error('Requires UID');
                process.exit(1);
            }
        }
    }
} else {
    console.error(HELP_TEXT);
    process.exit(1);
}