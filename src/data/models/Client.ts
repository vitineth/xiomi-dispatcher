import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class Client {
    /**
     * The unique UUID to identify this client - this should be used during authentication of notify requests
     */
    @PrimaryGeneratedColumn("uuid")
    uid!: string

    /**
     * The public key portion of a public private key RSA pair which should be used to verify digital signatures
     * in notify issues
     */
    @Column("text")
    publicKey!: string

    /**
     * The purpose for which this client was created
     */
    @Column("text")
    purpose!: string

    /**
     * The timestamp at which this client was deactivated, or null if it is currenty in use
     */
    @Column("bigint", {nullable: true})
    deactivated!: number | null | undefined

    /**
     * The timestamp at which this client was created
     */
    @Column("bigint")
    created!: number
}