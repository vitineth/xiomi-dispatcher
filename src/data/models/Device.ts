import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class Device {
    /**
     * The unique identifier for this device
     */
    @PrimaryGeneratedColumn("increment")
    uid!: number;

    /**
     * The FCM identifier of this device which can be used to target the device in messages
     */
    @Column("text")
    identifier!: string;

    /**
     * When this device was registered for the first time
     */
    @Column("bigint")
    registered!: number;

    /**
     * When this client last checked in
     */
    @Column("bigint")
    checkin!: number;
}