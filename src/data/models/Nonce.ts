import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class Nonce {
    /**
     * Then unique identifier for this nonce record
     */
    @PrimaryGeneratedColumn("increment")
    uid!: number;

    /**
     * The timestamp at which this nonce was used
     */
    @Column("bigint")
    used!: number;

    /**
     * The nonce value which was used
     */
    @Column("bigint")
    nonce!: number;
}