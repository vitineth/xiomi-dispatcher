import {DataSource} from "typeorm";
import {Client} from "./models/Client";
import {Nonce} from "./models/Nonce";
import {Device} from "./models/Device";

const source = new DataSource({
    type: "sqlite",
    database: "data/xiomi-dispatcher.sqlite",
    entities: [Client, Nonce, Device],
    synchronize: true,
    migrations: ["migrations/*.ts"],
    migrationsTableName: "migrations",
    logging: false,
});

export default source.initialize().then(() => source.synchronize().then(() => source));